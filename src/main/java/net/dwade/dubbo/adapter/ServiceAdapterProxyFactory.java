package net.dwade.dubbo.adapter;

/**
 * 服务层动态代理工厂接口，用于根据接口类动态生成实现类
 * @author huangxf
 * @date 2017年7月24日
 */
public interface ServiceAdapterProxyFactory {
	
	/**
	 * 服务提供者动态代理
	 * @param delegate	实际上被调用的对象，或者对应实现类的beanName，例如PaymentService的实现类
	 * @param interfaceClass	接口类，由该接口动态生成代理类，eg:PaymentServiceStringAdapter
	 * @param adapterInterfaceClass
	 * @return
	 */
	public Object createProviderProxy(Object delegate, Class<?> interfaceClass, Class<?> adapterInterfaceClass);

	/**
	 * 服务消费者动态代理，eg:PaymentService-->PaymentServiceStringAdapter，需要动态生成PaymentService的实现类，
	 * 在实现类中去调用PaymentServiceStringAdapter的实现类对应的某个方法
	 * @param delegate	实际上由该对象去调用provider服务，eg:PaymentServiceStringAdapter的实现类(或动态代理类)
	 * @param interfaceClass	接口类，由该接口动态生成代理类，eg:PaymentService
	 * @param adapterInterfaceClass
	 * @return
	 */
	public Object createConsumerProxy(Object delegate, Class<?> interfaceClass, Class<?> adapterInterfaceClass);

}

package net.dwade.dubbo.adapter.internal;

import java.lang.reflect.Method;

/**
 * rest、esb、dubbo之间调用的参数处理与转换接口
 */
public interface ServiceRequestResponseProcessor {
	
	/**
	 * esb --> dubbo，将esb调用dubbo的入参转换成dubbo接口需要的参数
	 * @param target	服务层对应的Method，eg：PaymentService#payOff
	 * @param requestString		ESB请求dubbo服务的String参数
	 * @return		服务层调用需要的入参，一般情况只有一个参数
	 */
	public Object[] resolveProviderArguments(Method target, String requestString);

	/**
	 * esb <-- dubbo, 处理dubbo接口的返回值，将接口出参转换为esb的返回值
	 * @param method
	 * @param returnValue
	 * @return
	 */
	public String handleProviderReturnValue(Method method, Object returnValue);

	/**
	 * rest --> esb，将consumer接口的调用参数，转换成调用esb的入参
	 * @param method	dubbo接口对应的方法，eg:PayentService#payOff
	 * @param arguments	dubbo接口调用的入参
	 * @return	调用ESB的入参
	 */
	public String handleConsumerArguments(Method method, Object[] arguments);

	/**
	 * rest <-- esb，将esb返回的数据转换成接口的出参，如果原有的接口返回异常，该方法也需要抛出异常
	 * @param method
	 * @param stringResponse
	 * @return
	 */
	public <T> Object resolveConsumerReturnValue(Method method, String stringResponse);
	
}

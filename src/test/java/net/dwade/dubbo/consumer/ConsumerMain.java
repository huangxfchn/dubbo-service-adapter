package net.dwade.dubbo.consumer;

import com.alibaba.fastjson.JSONObject;
import net.dwade.dubbo.api.JsonResult;
import net.dwade.dubbo.api.PaymentService;
import net.dwade.dubbo.api.TradeRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ConsumerMain {
	
	private static Logger logger = LoggerFactory.getLogger( ConsumerMain.class );

	public static void main(String[] args) {
		ConfigurableApplicationContext context = new ClassPathXmlApplicationContext( "classpath*:**/consumer/application-*.xml" );
		PaymentService paymentService = context.getBean( PaymentService.class );
		TradeRequest request = new TradeRequest();
		request.setTradeId( "789456123" );
		request.setMoney( 10000 );
		try {
			JsonResult result = paymentService.pay( request );
			logger.info( "Result:{}", JSONObject.toJSONString( result ) );
		} catch (Exception e) {
			e.printStackTrace();
		}
		context.close();
		System.exit( 0 );
	}

}

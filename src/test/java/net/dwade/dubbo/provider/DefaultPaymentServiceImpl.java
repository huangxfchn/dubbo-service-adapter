package net.dwade.dubbo.provider;

import net.dwade.dubbo.api.JsonResult;
import net.dwade.dubbo.api.PaymentService;
import net.dwade.dubbo.api.TradeRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service("paymentService")
public class DefaultPaymentServiceImpl implements PaymentService {

	private static final Logger logger = LoggerFactory.getLogger(DefaultPaymentServiceImpl.class);

	@PostConstruct
	public void init() {
		logger.info("init");
	}

	@Override
	public JsonResult pay(TradeRequest request) {
		return new JsonResult( 123, null );
	}

	@Override
	public JsonResult refund(TradeRequest request) {
		return new JsonResult();
	}

}

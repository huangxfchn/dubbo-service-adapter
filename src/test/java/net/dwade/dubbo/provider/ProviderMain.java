package net.dwade.dubbo.provider;

import net.dwade.dubbo.api.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.concurrent.CountDownLatch;

public class ProviderMain {
	
	private static CountDownLatch latch = new CountDownLatch(1);
	
	private static Logger logger = LoggerFactory.getLogger( ProviderMain.class );

	public static void main(String[] args) throws Exception {

		String[] configs = new String[]{"classpath*:**/provider/application-*.xml"};
		ApplicationContext context = new ClassPathXmlApplicationContext( configs );
		context.getBean( "paymentService", PaymentService.class ).pay(null);
		
		/*Object proxy = context.getBean( "paymentService$StringAdapter" );
		Method method = proxy.getClass().getMethod( "pay", String.class );
		
		// 模拟dubbo consumer调用
		TradeRequest request = new TradeRequest();
		request.setTradeId( "123456789" );
		request.setMoney( 100 );
		String stringArgment = JSONObject.toJSONString( request );
		Object result = method.invoke( proxy, stringArgment );
		logger.info( "result:{}", result );*/

		latch.await();
	}

}

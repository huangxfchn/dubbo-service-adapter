package net.dwade.dubbo.provider;

import net.dwade.dubbo.api.ItemRequest;
import net.dwade.dubbo.api.ItemService;
import net.dwade.dubbo.api.JsonResult;
import org.springframework.stereotype.Service;

@Service("itemService")
public class ItemServiceImpl implements ItemService {

	@Override
	public JsonResult queryList(ItemRequest request) {
		return new JsonResult();
	}

	@Override
	public JsonResult queryInfo(ItemRequest request) {
		return new JsonResult();
	}

}

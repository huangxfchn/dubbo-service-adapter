package net.dwade.dubbo.api;

public class JsonResult implements java.io.Serializable {
	
	private static final long serialVersionUID = 977005450142716286L;

	/**
     * JSON结果成功状态值：1。
     */
    public static final int STATUS_SUCCESS = 1;

    /**
     * JSON结果失败状态值：0。
     */
    public static final int STATUS_FAIL = 0;

    /**
     * JSON结果错误状态值：-1。
     */
    public static final int STATUS_ERROR = -1;
    
    /**
     * 状态值。表示请求成功、失败、错误等状态。
     */
    private int status = STATUS_SUCCESS;
	
    /**
     * 消息
     */
	private String msg;
	
	/**
	 * Json数据
	 */
	private Object data;
	
	public JsonResult() {
		
	}

	public JsonResult(int status, String msg, Object data) {
		super();
		this.status = status;
		this.msg = msg;
		this.data = data;
	}

	public JsonResult(int status, String msg) {
		super();
		this.status = status;
		this.msg = msg;
	}
	
	public JsonResult(int status) {
		this.status = status;
	}
	
	private String getDefaultMsg() {
		if ( STATUS_SUCCESS == this.status ) {
			return "请求成功";
		} else if ( STATUS_FAIL == this.status ) {
			return "结果失败";
		} else if ( STATUS_ERROR == this.status ) {
			return "发生错误";
		} else {
			return null;
		}
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMsg() {
		if ( msg == null ) {
			this.msg = getDefaultMsg();
		}
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}

package net.dwade.dubbo.api;

public class ItemRequest implements java.io.Serializable {
	
	private static final long serialVersionUID = -8434068960342939912L;

	private String keyWord;
	
	private String id;

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}

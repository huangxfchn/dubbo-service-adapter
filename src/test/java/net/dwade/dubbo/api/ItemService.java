package net.dwade.dubbo.api;


public interface ItemService {
	
	public JsonResult queryList(ItemRequest request);

	public JsonResult queryInfo(ItemRequest request);

}

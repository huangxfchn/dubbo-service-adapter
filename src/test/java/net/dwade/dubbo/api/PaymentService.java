package net.dwade.dubbo.api;


public interface PaymentService {
	
	/**
	 * 付款 
	 * @author huangxf
	 * @param request
	 * @return JsonResult
	 */
	public JsonResult pay(TradeRequest request);

	/**
	 * 退款
	 * @param request
	 * @return JsonResult
	 */
	public JsonResult refund(TradeRequest request);

}

package net.dwade.dubbo.api;

public class TradeRequest implements java.io.Serializable {
	
	private static final long serialVersionUID = -6687232046990516147L;

	private String tradeId;
	
	private int money;

	public String getTradeId() {
		return tradeId;
	}

	public void setTradeId(String tradeId) {
		this.tradeId = tradeId;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

}

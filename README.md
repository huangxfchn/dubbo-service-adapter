# dubbo-service-adapter
* 博客地址：https://blog.csdn.net/Dwade_mia/article/details/79383278
* 对spring源码进行扩展，修改dubbo Consumer端的动态代理，使其调用esb服务
* 利用asm工具，根据原有的接口，生成新的接口，满足esb的要求，注意：新的接口要求入参和出参都是String
* provider端会同时暴露两套服务，一个原生接口，另外一个是asm生成的StringAdapter接口
* consumer端会替换dubbo的动态代理实现，使用调用esb逻辑的动态代理

# 代码说明
代码目录结构如下所示：
```
├─src
│  ├─main
│  │  ├─java
│  │  │  └─net
│  │  │      └─dwade
│  │  │          └─dubbo
│  │  │              └─adapter
│  │  │                  │  DubboConsumerAdapterSupport.java    consumer端增强
│  │  │                  │  DubboProviderAdapterSupport.java    provider端增强
│  │  │                  │  ServiceAdapterProxyFactory.java     动态代理工厂，用于生成动态代理实现类
│  │  │                  │  StringAdapterInterfaceUtils.java    asm工具，用于动态生成字节码
│  │  │                  ├─internal
│  │  │                  │      EsbParamConvertHandler.java     esb接口的参数处理接口
│  │  │                  │      EsbServiceAdapterProxyFactory.java  esb动态代理类
│  │  │                  │      ServiceRequestResponseProcessor.java
│  │  │                  └─utils
│  │  │                          JsonUtils.java
│  │  └─resources
│  │          log4j2.xml
│  └─test
│      └─java
│          └─net
│              └─dwade
│                  └─dubbo
│                      ├─api  定义接口和bean
│                      │      ItemRequest.java
│                      │      ItemService.java
│                      │      JsonResult.java
│                      │      PaymentService.java
│                      │      TradeRequest.java
│                      ├─consumer   consumer端测试代码
│                      │      application-consumer-adapter.xml
│                      │      application-consumer.xml
│                      │      ConsumerMain.java
│                      └─provider   provider端测试代码
│                              application-provider-adapter.xml
│                              application-provider.xml
│                              DefaultPaymentServiceImpl.java
│                              ItemServiceImpl.java
│                              ProviderMain.java
```
